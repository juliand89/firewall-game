/* The stuff that maintains a network topology and tests if a packet can be
 * routed from source to destination based on the rules on the nodes.
 */

// TODO: add fromJSON, follow model instantiation guidelines as specified in our
// docs.
var Network = Backbone.Model.extend({
    initialize: function (attributes) {
        attributes = attributes || { nodes: [], connections: [] };

        var ipToNode = {}; // maps IP address to node object
        var graph = new Graph();
        
        _.each(attributes.nodes, function (node) {
            ipToNode[node.get('ip')] = node;
        });

        _.each(attributes.connections, function (con) {
            var ipA = con[0];
            var ipB = con[1];
            graph.addEdge(ipA, ipB);
        });

        this.set('ipToNode', ipToNode);
        this.set('graph', graph);
    },

    send: function (packet) {
        var src = packet.get('srcIP');
        var dst = packet.get('dstIP');
        var path = this.get('graph').getPath(src, dst);

        // go through path and test each nodes rule list against packet
        var ipToNode = this.get('ipToNode');
        return _.all(path, function (ip) {
            return ipToNode[ip].get('rules').decide(packet) === 'allow';
        });
    }, 
    //this function takes Node objects as parameters
    connect: function (nodeA, nodeB) {
        var graph = this.get('graph');
        graph.addEdge(nodeA.get('ip'), nodeB.get('ip'));
        this.set('graph', graph);
    },
    addRule: function (ip, rule) {
        var ipToNode = this.get('ipToNode');
        ipToNode[ip].get('rules').addRule(rule);
    }
    
});
