describe('a rule', function () {
    var rule;

    beforeEach(function () {
        rule = new Rule();
    });

    it('has sane default values', function () {
        expect(rule.get('srcIP')).toBeNull();
        expect(rule.get('dstIP')).toBeNull();
        expect(rule.get('srcPort')).toBeNull();
        expect(rule.get('dstPort')).toBeNull();
        expect(rule.get('action')).toEqual('allow');
    });

    it('can be initialized from values in an object', function () {
        rule = new Rule({
            dstPort: 80
        });

        expect(rule.get('srcIP')).toBeNull();
        expect(rule.get('srcPort')).toBeNull();
        expect(rule.get('dstPort')).toEqual(80);
    });

    describe('for denying destination port 80', function () {
        beforeEach(function () {
            rule = new Rule({
                dstPort: 80,
                action: 'deny'
            });
        });

        it('should match packet with destination port 80', function (){
            var packet = new Packet({
                srcIP: '192.168.1.100', dstIP: '192.168.1.101',
                srcPort: 1234, dstPort: 80
            });

            expect(rule.match(packet)).toEqual(true);
        });

        it('should not match a packet with destination port 443', function (){
            var packet = new Packet({
                srcIP: '192.168.1.100', dstIP: '192.168.1.101',
                srcPort: 1234, dstPort: 443
            });

            expect(rule.match(packet)).toEqual(false);
        });
    });
});

describe('a rule can contain any source or destination ip', function () {
    it('can accept traffic based on any', function () {
       var rule = new Rule({
            srcIP: 'any',
            dstIP: '1.1.1.10',
            action: 'accept'
        });
        
        var packet1 = new Packet({
            srcIP: '192.168.1.100', dstIP: '1.1.1.10',
            srcPort: '80', dstPort: '80'
        });
        
        var packet2 = new Packet({
            srcIP: '192.168.1.101', dstIP: '1.1.1.10',
            srcPort: '80', dstPort: '80'
        });
        
        expect(rule.match(packet1)).toEqual(true);
        expect(rule.match(packet2)).toEqual(true);
    
    });
    
    it('matches when packet comes from any ip', function () {
        var rule = new Rule({
            srcIP: 'any',
            dstIP: '1.1.1.1',
            action: 'deny'        
        });
        
        var packet1 = new Packet({
            srcIP: 'any',
            dstIP: '1.1.1.1',
            srcPort: '80',
            dstPort: '80'
        });
        expect(rule.match(packet1)).toEqual(true);
    });

}); 


describe('a rule list', function () {
    var list;
});
