'use strict';

describe('a network', function () {
    var network;


    

    describe('created with nodes and connections', function () {
        var packet;

        beforeEach(function () {
            network = new Network({
                nodes: [
                    new Node({
                        ip: '1.1.1.1', 
                        name: 'router'
                       
                    }),
                    new Node({ip: '1.1.1.2', name: 'client A'}),
                    new Node({ip: '1.1.1.3', name: 'client B'})
                ],
                connections: [
                    ['1.1.1.1', '1.1.1.2'], // connect client A to router
                    ['1.1.1.1', '1.1.1.3']  // connect client B to router
                ]
            });

            packet = new Packet({
                type: 'tcp',
                srcIP: '1.1.1.2',
                dstIP: '1.1.1.3',
                srcPort: 80,
                dstPort: 80
            });
        });

        it("allows permitted traffic between connected nodes", function () {
            expect(network.send(packet)).toEqual(true);
        });

        it('denies traffic according to node\'s rules', function () {
            var rule = new Rule({
                            srcIP: '1.1.1.2',
                            dstIP: '1.1.1.3',
                            srcPort: 80,
                            dstPort: 80,
                            action: 'deny'
            });
            window.net = network;
            network.addRule('1.1.1.1', rule); //add rule to router
            expect(network.send(packet)).toEqual(false);
        });
    });
    
    describe('can use the network wrapper function to modify the graph', function () {
        var network;
        var packet;
        beforeEach(function () {
            network = new Network({
                nodes: [
                    new Node({
                        ip: '1.1.1.1', 
                        name: 'router'
                    })
                ]
            });
            
            packet = new Packet({
                type: 'tcp',
                srcIP: '1.1.1.2',
                dstIP: '1.1.1.3',
                srcPort: 80,
                dstPort: 80
            });
        });
    
        it('can add connections to graph', function () {
            var node1 = new Node({ip: '1.1.1.2', name: 'client A'});
            var node2 = new Node({ip: '1.1.1.3', name: 'client B'});
            var router = network.get('ipToNode')['1.1.1.1'];
            network.connect(node1, router);
            network.connect(node2, router);
            expect(network.get('graph').adj('1.1.1.1')).toEqual(['1.1.1.2','1.1.1.3']);
        });
        //this will eventually be tested
        xit('will not allow cycles to be added to the network', function () {});
    
    });
});
