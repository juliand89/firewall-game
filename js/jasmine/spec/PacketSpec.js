describe('a packet', function () {
    var packet;

    xit('requires specifying certain values when constructing', function () {
        expect(function () {
            packet = new Packet({
                srcPort: 1234
            });
        }).toThrow();

        expect(function () {
            packet = new Packet({
                srcIP: '192.168.1.100',
                dstIP: '192.168.1.102',
                srcPort: 3923,
                dstPort: 80
            });
        }).not.toThrow();
    });

    it('has sane default values', function () {
        packet = new Packet({
            srcIP: '192.168.1.100',
            dstIP: '192.168.1.102',
            srcPort: 3923,
            dstPort: 80
        });

        expect(packet.get('type')).toEqual('tcp');
    });
});
