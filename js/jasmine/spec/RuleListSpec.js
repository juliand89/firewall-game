describe('a rule list', function () {
    var list;

    var packetA = new Packet({
        srcIP: '175.168.3.103',
        dstIP: '8.8.8.8',
        srcPort: 4312,
        dstPort: 53
    });
    var packetB = new Packet({
        srcIP: '192.168.1.100',
        dstIP: '192.168.1.110',
        srcPort: 5352,
        dstPort: 22
    });

    describe('created with no rules', function () {
        beforeEach(function () {
            list = new RuleList([], {});
        });

        it('has a default allow policy', function () {
            expect(list.policy).toEqual('allow');
        });

        it('allows various packets', function () {
            expect(list.decide(packetA)).toEqual('allow');
            expect(list.decide(packetB)).toEqual('allow');
        });
    });

    describe('created with no rules and a deny policy', function () {
        beforeEach(function () {
            list = new RuleList([], {
                policy: 'deny'
            });
        });

        it('has a deny policy', function () {
            expect(list.policy).toEqual('deny');
        });

        it('denies various packets', function () {
            expect(list.decide(packetA)).toEqual('deny');
            expect(list.decide(packetB)).toEqual('deny');
        });
    });

    describe('created with some specific rules', function () {
        beforeEach(function () {
            list = new RuleList([
                    new Rule({
                        dstIP: '4.4.4.4',
                        dstPort: 22,
                        action: 'deny'
                    }),
                    new Rule({
                        srcIP: '4.4.4.4',
                        dstPort: 80,
                        action: 'deny'
                    })
            
            ]);
        });

        it('blocks 4.4.4.4 from serving on 22', function () {
            var packet = new Packet({
                srcIP: '34.5.345.45',
                srcPort: 7463,
                dstIP: '4.4.4.4',
                dstPort: 22
            });
            expect(list.decide(packet)).toEqual('deny');
        });

        it('blocks 4.4.4.4 from requesting on 80', function () {
            var packet = new Packet({
                srcIP: '4.4.4.4',
                srcPort: 39424,
                dstIP: '34.5.345.45',
                dstPort: 80
            });
            expect(list.decide(packet)).toEqual('deny');
        });

        it('allows 4.4.4.4 to serve on 80', function () {
            var packet = new Packet({
                srcIP: '34.5.345.45',
                srcPort: 39424,
                dstIP: '4.4.4.4',
                dstPort: 80
            });
            expect(list.decide(packet)).toEqual('allow');
        });
    });

    describe("created empty and uses addRule() to update RuleList", function () {
        beforeEach(function() {
            list = new RuleList();
        });

        it('adds a rule to block port 80',function () {
            // create rule to block port 80
            var rule = new Rule({
                srcIP: '1.1.1.1',
                srcPort: 80,
                dstIP: '1.1.1.2',
                dstPort: 80,
                action: 'deny'
            });

            // create packet to be denied
            var packet = new Packet({
                type: 'tcp',
                srcIP: '1.1.1.1',
                srcPort: 80,
                dstIP: '1.1.1.2',
                dstPort: 80
            });

            list.addRule(rule);
            expect(list.decide(packet)).toEqual('deny');
        });
    });
});
