describe('a node', function () {
    var node;

    beforeEach(function () {
        node = new Node({ ip: '192.168.1.1' });
    });

    xit('should require that an IP address is set', function () {
        expect(function () {
            new Node();
        }).toThrow();
    });

    it('gets an empty rule list', function () {
        expect(node.get('rules') instanceof RuleList).toBeTruthy();
    });

    xit('gets a generic name if one is not given', function () {
        expect(node.get('name')).toBeTruthy();
    });

    xit('gets a generic name that is different from other node\'s generic names', function () {
        nodeA = new Node({ ip: '192.168.1.1' });
        nodeB = new Node({ ip: '192.168.1.2' });
        expect(nodeA.get('name')).not.toEqual(nodeB.get('name'));
    });
});
