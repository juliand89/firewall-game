var Rule = Backbone.Model.extend({
    defaults: {
        type: 'tcp',
        // null means any
        srcIP: null,
        dstIP: null,
        srcPort: null,
        dstPort: null,

        action: 'allow'
    },

    // Return true if the rule matches the packet, false otherwise.
    match: function (packet) {
        var criteria = ['type','srcIP', 'dstIP', 'srcPort', 'dstPort'];
        var that = this;
        return _.chain(criteria)
            // remove criteria with unspecified values
            .filter(function (crit) { return that.get(crit) !== null; })
            .filter(function (crit) { return that.get(crit) !== 'any';})
            // ensure rule and packet agree on all criteria
            .all(function (crit) {
                return that.get(crit) === packet.get(crit);
            }, _.identity)
            .value();
    }
});
