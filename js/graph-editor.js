'use strict';

// graph view and controller. allows user to edit the connections in the graph
// and responds to node add/remove events by adding/removing nodes from the
// graph.
window.GraphView = Backbone.View.extend({
    tagName: 'canvas',

    initialize: function (options) {
        // we bind the events here instead of the events array because we
        // want them delegated from the window to allow mouse dragging to
        // provide feedback even when the dragging cursor has left the canvas.
        this.$el.on('mousedown', _.bind(this.onMouseDown, this));
        $(window).on('mousemove', _.bind(this.onMouseMove, this));
        $(window).on('mouseup', _.bind(this.onMouseUp, this));
        this.ctx = this.el.getContext('2d');

        this.width = options.width || 700;
        this.height = options.height || 500;

        this.el.width = this.width;
        this.el.height = this.height;

        this.graph = options.graph;
        this.nodes = options.nodes;

        // state variables for UI interaction
        
        this.nodeDim = {}; // width and height of nodes is used for hit-testing

        this.dragging = false;
        this.draggedNode = null;
        // the offset between Node center and cursor. enables dragging a node
        // by it's corner without the node jumping so its center is below the
        // cursor.
        this.offset = {};

        // the click event will trigger even when mouse up is part of a
        // dragging operation. Since we don't want to select a node after a
        // drag operation, we watch the down and up locations and only register
        // a selection if they are within a certain small delta (like a regular
        // click).
        this.mouseDownPos = null; // pos of mouse down event
        this.mouseUpPos = null; // pos of mouse up event
        this.selectedNode = null;
    },

    render: function () {
        var ctx = this.ctx;
        var graph = this.graph;
        var nodes = this.graph.getNodes();

        ctx.clearRect(0, 0, this.width, this.height);
        ctx.strokeStyle = 'black';
        ctx.fillStyle = 'lightgrey';

        // draw connections
        ctx.lineWidth = 2;
        ctx.beginPath();
        _.each(nodes, function (node) {
            var nodePos = graph.getPos(node);
            _.each(graph.adj(node), function (neighbor) {
                var neighPos = graph.getPos(neighbor);
                ctx.moveTo(nodePos.x, nodePos.y);
                ctx.lineTo(neighPos.x, neighPos.y);
            });
        });
        ctx.stroke();

        // draw nodes
        ctx.lineWidth = 1;
        ctx.font = '16px sans-serif';
        ctx.textAlign = 'center';
        var that = this;
        _.each(this.graph.getNodes(), function (node) {
            if (that.selectedNode === node || that.draggedNode === node) {
                ctx.fillStyle = 'white';
            } else {
                ctx.fillStyle = 'lightgrey';
            }
            var pos = graph.getPos(node);
            var nodeObj = that.nodes.get(node);
            var nodeName = nodeObj.get('name');
            var nodeIP = nodeObj.get('ip');

            // max text width will determine width of nod
            var tw = _.chain([nodeName, nodeIP])
                .map(_.bind(ctx.measureText, ctx))
                .pluck('width')
                .max()
                .value();

            var w = tw + 16; // width of node
            var h = 48; // height of node
            that.nodeDim[node] = {'w': w, 'h': h};
            // the extra 0.5 pixel-aligns the 1px border
            var xLeft = pos.x - Math.round(w / 2) + 0.5;
            var yTop = pos.y - Math.round(h / 2) + 0.5;
            ctx.beginPath();
            ctx.fillRect(xLeft, yTop, w, h);
            ctx.strokeRect(xLeft, yTop, w, h);
            ctx.fillStyle = 'black';

            ctx.textBaseline = 'bottom';
            ctx.fillText(nodeName, pos.x, pos.y);

            ctx.textBaseline = 'top';
            ctx.fillText(nodeIP, pos.x, pos.y + 4);
        });

        return this;
    },

    nodeUnderCursor: function (e) {
        var click = this.getClickPoint(e);

        // find clicked node (if any)
        var that = this;
        var isMouseOver = function (node) {
            var nodePos = that.graph.getPos(node);
            var dx = Math.abs(click.x - nodePos.x);
            var dy = Math.abs(click.y - nodePos.y);
            var nodeDim = that.nodeDim[node];
            return (dx < nodeDim.w / 2 && dy < nodeDim.h / 2);
        };

        return _.find(this.graph.getNodes(), isMouseOver) || null;
    },

    onMouseDown: function (e) {
        this.mouseDownPos = this.getClickPoint(e);
        var clickedNode = this.nodeUnderCursor(e);

        if (clickedNode) {
            this.dragging = true;
            this.draggedNode = clickedNode;

            var nodePos = this.graph.getPos(clickedNode);
            this.offset.x = nodePos.x - this.mouseDownPos.x;
            this.offset.y = nodePos.y - this.mouseDownPos.y;
        }
        
        this.render();
    },

    onMouseMove: function (e) {
        if (this.dragging) {
            var clamp = function (input, min, max) {
                if (input < min) { return min; }
                else if (input > max) { return max; }
                else { return input; }
            };
            var click = this.getClickPoint(e);
            var newPos = {
                x: clamp(click.x + this.offset.x, 0, this.width),
                y: clamp(click.y + this.offset.y, 0, this.height)
            };
            this.graph.setPos(this.draggedNode, newPos);

        this.render();
        }
    },

    onMouseUp: function (e) {
        this.mouseUpPos = this.getClickPoint(e);
        this.dragging = false;
        this.draggedNode = null;

        var down = this.mouseDownPos;
        var up = this.mouseUpPos;
        // if click start and end are within four pixels, treat it as a
        // node selection event
        if (Math.sqrt(Math.pow(down.x - up.x, 2) + Math.pow(down.y - up.y, 2)) < 4) {
            var clickedNode = this.nodeUnderCursor(e);
            var lastSelected = this.selectedNode;

            if (this.selectedNode === clickedNode) {
                this.selectedNode = null;
            } else {
                this.selectedNode = clickedNode;
            }

            if (lastSelected && this.selectedNode) {
                if (this.graph.isEdge(lastSelected, this.selectedNode)) {
                    this.graph.removeEdge(lastSelected, this.selectedNode);
                } else {
                    this.graph.addEdge(lastSelected, this.selectedNode);
                }

                this.selectedNode = null;
            }
        }

        this.render();
    },

    getClickPoint: function (e) {
        var x = e.pageX - this.ctx.canvas.offsetLeft;
        var y = e.pageY - this.ctx.canvas.offsetTop;
        return {'x': x, 'y': y};
    }
});

$(function () {
    var graph = new Graph();

    graph.addNode('a');
    graph.addNode('b');
    graph.addNode('c');
    graph.addNode('d');

    graph.setPos('a', {x: 100, y: 50});
    graph.setPos('b', {x: 150, y: 60});
    graph.setPos('c', {x: 400, y: 70});
    graph.setPos('d', {x: 250, y: 80});

    graph.addEdge('a', 'b');
    graph.addEdge('b', 'c');
    graph.addEdge('c', 'd');

    var nodes = new Nodes(
      [ { name: 'Fedora 17'
        , ip: '192.168.1.100'
        , id: 'a'
        }
      , { name: 'Ubuntu 12.04'
        , ip: '192.168.1.101'
        , id: 'b'
        }
      , { name: 'Node.js Server'
        , ip: '192.168.1.120'
        , id: 'c'
        }
      , { name: 'MongoDB Server'
        , ip: '192.168.1.130'
        , id: 'd'
        }
      ]
    );

    var graphView = new GraphView({
        graph: graph,
        nodes: nodes
    });

    $('body').append(graphView.render().el);
});
