var GameView = Backbone.View.extend({
    template: _.template($('#game-template').html()),
    render: function () {
        this.$el.html(this.template());

        (new ProgressView({ el: this.$el.find('#progress-area') })).render();
        (new ChallengeView({ el: this.$el.find('#challenge-area') })).render();
        (new RulesView({ el: this.$el.find('#rules-area') })).render();
        (new DiagramView({ el: this.$el.find('#diagram-area') })).render();
    },
});
