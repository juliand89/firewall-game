/* An _undirected_ graph structure. */

// May be called with `attributes`, a JSON object representing a graph. This is
// useful for reconstituting a graph from a JSON file.
//
// It is recommended to *not* use a hand-written `attributes` because the graph
// object's methods depends on certain invariants that are maintained by the
// graph's mutator methods.
//
// Nodes are specified by strings. There is no 'node object', per se.
var Graph = function (attributes) {
    attributes = attributes || {};

    this.n = attributes.n || 0; // number of nodes
    this.e = attributes.e || 0; // number of edges
    this.g = attributes.g || {}; // adjacency list structure

    // the x and y positions of the nodes
    this.pos = attributes.pos || {};
};

Graph.prototype = {
    getNodes: function () {
        return _.keys(this.g);
    },

    // return an object with the x and y position of the given node
    getPos: function (node) {
        return this.pos[node];
    },
    
    // set the position object for the given node
    setPos: function (node, pos) {
        this.pos[node] = pos;
    },

    // get list of nodes adjacent to `node`
    adj: function (node) {
        return this.g[node];
    },

    addNode: function (node){
        if (this.g[node] === undefined) {
            this.g[node] = [];
            this.n++;
        }
    },

    removeNode: function (node) {
        var entriesRemoved = 0;

        // remove the node's position entry
        delete this.pos[node];

        // remove this node's adjacency lists
        if (this.g[node]) {
            entriesRemoved += this.g[node].length;
            delete this.g[node];
        }

        // remove this node's appearance in other's adjacency lists
        var that = this;
        _.each(_.keys(this.g), function (key) {
            that.g[key] = _.reject(that.g[key], function (n) {
                if (n === node) {
                    entriesRemoved++;
                }
                return (n === node);
            });
        });

        // decrement the node counter
        this.n--;
        // because the links are double-counted, we divide by two
        this.e -= entriesRemoved / 2;
    },

    // add an undirected edge between two nodes
    // TODO: how do we want to handle adding the same edge twice?
    addEdge: function (nodeA, nodeB) {
        this.addNode(nodeA);
        this.addNode(nodeB);
        this.g[nodeA].push(nodeB);
        this.g[nodeB].push(nodeA);
        this.e++;
    }, 

    removeEdge: function (nodeA, nodeB) {
        this.g[nodeA] = _.filter(this.g[nodeA], function (n) {
            return n !== nodeB;
        });
        this.g[nodeB] = _.filter(this.g[nodeB], function (n) {
            return n !== nodeA;
        });
        this.e--;
    },

    isEdge: function (nodeA, nodeB) {
        return _.include(this.g[nodeA], nodeB);
    },

    // return true if graph is a tree
    isTree: function () {
        // a graph is a tree if it has n-1 edges and is connected

        // ensure graph has n-1 edges
        if (this.e !== (this.n - 1)) {
            return false;
        }

        // ensure graph is connected
        var visited = {};
        var numVisited = 0;
        var nodes = _.keys(this.g);
        _.each(nodes, function (node) {
            visited[node] = false;
        });

        var dfs = function (node) {
            visited[node] = true;
            numVisited++;
            _.each(this.adj(node), function (neighbor) {
                if (!visited[neighbor]) {
                    dfs(neighbor);
                }
            });
        };
        dfs = _.bind(dfs, this);

        dfs(nodes[0]);

        // graph is connected if all nodes have been visited
        return numVisited === this.n;
    },

    getPath: function (src, dst) {
        // warn if pathfinding on a non-tree graph. Only a tree guarantees
        // unique shortest paths.
        if (!this.isTree) {
            console.log("Warning: pathfinding on a non-tree.");
        }

        var visited = {};
        // pred[x] gives the parent node of x (wrt. the bfs tree)
        var pred = {};
        var nodes = _.keys(this.g);
        _.each(nodes, function (node) {
            visited[node] = false;
        });

        // the bfs traversal
        var queue = [src];
        visited[src] = true;
        while (queue.length > 0) {
            var cur = queue.pop();
            _.each(this.adj(cur), function (neighbor) {
                if (!visited[neighbor]) {
                    pred[neighbor] = cur;
                    visited[neighbor] = true;
                    queue.unshift(neighbor);
                }
            });
        }

        // recover the path from the traversal data
        var path = [];
        for (cur = dst; pred[cur] !== undefined; cur = pred[cur]) {
            path.unshift(cur);
        }
        path.unshift(cur);

        return path;
    }
};
