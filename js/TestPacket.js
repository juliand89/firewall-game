var TestPacket = Backbone.Model.extend({
    defaults: function () {
        return {
            packet: new Packet(),
            shouldPass: true,
            passMsg: '',
            failMsg: ''
        };
    },

    // The built-in clone method calls the model's constructor with the model's
    // `attributes` attribute passed in. This causes the model constructor to
    // receive a packet attribute in the form of a Packet instance instead of a
    // JSON object. (the to-be-cloned mode's constructor converted the JSON
    // object it recieved into a packet instance)
    clone: function () {
        var attributes = _.clone(this.attributes);
        attributes.packet = attributes.packet.toJSON();
        return new TestPacket(attributes);
    }
});

var TestPackets = Backbone.Collection.extend({
    model: TestPacket
});
