fg = {}; // the firewall game globals object.

var ProgressView = Backbone.View.extend({
    template: _.template($('#progress-template').html()),
    render: function () {
      this.$el.html(this.template());
    },
});

var ChallengeView = Backbone.View.extend({
    template: _.template($('#challenge-template').html()),
    render: function () {
      this.$el.html(this.template());
    },
});

var RulesView = Backbone.View.extend({
    template: _.template($('#rules-template').html()),
    render: function () {
      this.$el.html(this.template());
    },
});

var DiagramView = Backbone.View.extend({
    template: _.template($('#diagram-template').html()),
    render: function () {
      this.$el.html(this.template());
    },
});

$(function () {
    fg.g = new GameState();
    fg.mainRegion = new RegionManager($('#content-area'));

    fg.gameView = new GameView();
    fg.loadView = new LoadView();

    fg.mainRegion.show(fg.gameView);
    fg.mainRegion.show(fg.loadView);
});
