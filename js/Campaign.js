var Campaign = Backbone.Model.extend({
    defaults: function () {
        return {
            title: '',
            authors: '',
            description: '',
            debrief: '',
            challenges: new Challenges(),
            nodes: new Nodes(),
            graph: new Graph()
        };
    },

    initialize: function (attributes) {
        if (!(this.get('challenges') instanceof Challenges)) {
            this.set('challenges', new Challenges(attributes.challenges));
        }
        if (!(this.get('nodes') instanceof Nodes)) {
            this.set('nodes', new Nodes(attributes.nodes));
        }
        if (!(this.get('graph') instanceof Graph)) {
            this.set('graph', new Graph(attributes.graph));
        }
    }
});
