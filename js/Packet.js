var Packet = Backbone.Model.extend({
    defaults: function () {
        return {
            // these must be specified by the caller
            srcIP: null,
            dstIP: null,
            srcPort: null,
            dstPort: null,
            // a sane default
            type: 'tcp'
        };
    }
});

var Packets = Backbone.Collection.extend({
    model: Packet
});
