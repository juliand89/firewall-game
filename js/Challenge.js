var Challenge = Backbone.Model.extend({
    defaults: function () {
        return {
            prompt: '',
            solution: '',
            debrief: '',
            points: 0,
            testPackets: new TestPackets(),
            questions: new Questions()
        };
    },

    initialize: function (attributes) {
        if (!(this.get('testPackets') instanceof TestPackets)) {
            this.set('testPackets', new TestPackets(attributes.testPackets));
        }
        if (!(this.get('questions') instanceof Questions)) {
            this.set('testPackets', new Questions(attributes.questions));
        }
    }
});

var Challenges = Backbone.Collection.extend({
    model: Challenge
});
