// Inspired by;
// http://lostechies.com/derickbailey/2011/12/12/composite-js-apps-regions-and-region-managers/
//
// A region manager controls a dom element (referred to as a region), replacing
// the region's contents with those of views passed through the show method.
// Region managers will call open and close methods on the views it manages if
// those methods exist. Otherwise, it will simply call the given view's render
// method, and replace the region with view's element.


// Create a region manager, optionally give it a jQuery object to set the
// region. With no arguments, the region manager creates a new div for the
// region.
var RegionManager = function ($el) {
    this.currentView = null;
    this.$el = $el;
}

var RegionManager = function ($el) {
    this.$el = $el || $('div');
};

RegionManager.prototype.show = function (nextView) {
    // close the current view
    if (this.currentView && this.currentView.close) {
        this.currentView.close();
    }

    // open the nextView
    if (nextView && nextView.open) {
        nextView.open(this.$el);
    } else {
        nextView.render();
        this.$el.html(nextView.el);
    }

    this.currentView = nextView;
};
