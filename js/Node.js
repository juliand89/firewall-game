var Node = Backbone.Model.extend({
    defaults: function () {
        return {
            'name': '',
            'ip': '',
            // the node ID is what ties together nodes in a campaign's node
            // list and graph.
            'id': Node.getID(),
            'rules': new RuleList()
        };
    },

    clone: function () {
        var clone = Backbone.Model.prototype.clone.call(this);
        clone.id = Node.getID();
        // we also need to set the id as an attribute so that it's serialized
        // on export and not ignored.
        clone.set('id', clone.id);
        return clone;
    },

    initialize: function (attributes) {
        if (!(this.get('rules') instanceof RuleList)) {
            this.set('rules', new RuleList(attributes.rules, {}));
        }
    }
});

Node.getID = function () {
    // Math.random() is actually a POOR choice for generating unique IDs. But
    // for right now, it's easy. The random number is converted to hex for
    // aesthetics.
    return Math.random().toString(16).substr(2)
};


Nodes = Backbone.Collection.extend({
    model: Node
});
